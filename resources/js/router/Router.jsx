import React from 'react'
import {Routes, Route} from "react-router-dom"
import IndexTask from '../components/tasks/Index'
import NewTask from '../components/tasks/New'
import NotFound from '../components/Notfound'

const Router = () => {
    return (
     <div>
        <Routes>
            <Route path= "/" element = {<IndexTask/>}/>
            <Route path= "/task/new" element = {<NewTask/>}/>
            <Route path= "/*" element = {<NotFound/>}/>
        </Routes>
     </div>
    )

}

export default Router 