import './bootstrap';

import React from 'react'
import ReactDOM from 'react-dom/client'
import {BrowserRouter} from "react-router-dom"

import App from './components/App'

import Swal from 'sweetalert2/dist/sweetalert2.js'
import'sweetalert2/dist/sweetalert2.css'

ReactDOM.createRoot(document.getElementById('app')).render(
    <BrowserRouter>
    <App/>
    </BrowserRouter>
    
    
)
