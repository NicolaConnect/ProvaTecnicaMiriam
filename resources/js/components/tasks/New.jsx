import React, {useState} from "react"

const New = () => {
    

        const [name , setName] = useState("")
        const [description , setDescription] = useState("")
        const [deadline , setDeadline] = useState("")
        const [status , setStatus] = useState("")
        
    const createProduct = async (e) =>{
        e.preventDefault()
    }



    return(    
        <div className="container">
           <div className="tasks_create">
            <div className="titlebar">
                <div className="title_card">
                    <h2>Add Task</h2>
                    <i class=" icon_task fa-solid fa-list-check"></i>
                </div>
                </div>
            </div>
            

            
{/* card wrapper */}
            <div className="card_wrapper">
                <div className="wrapper_left">
                    <div className="card">

                        <h5>Name</h5>

                        <input type="text" value={name} onChange = {(event)=>{setName(event.target.value)}}/>
                        
                        <h5>Description</h5>

                        <textarea cols="10" rows="5" value={description} onChange = {(event)=>{setDescription(event.target.value)}}></textarea>
                        <div className="deadline">
                        <h5>Deadline</h5>    
                        <input type="date" value={deadline} onChange = {(event)=>{setDeadline(event.target.value)}} />

                         <h5 className="level_title">Status</h5>
                        <select name="choice" value={status} onChange = {(event)=>{setStatus(event.target.value)}}>
                        <option value="first">Completed</option>
                        <option value="second" selected>Not completed</option>
                        </select> 
                    </div>
                    <div className="btn_item">
                    <button className="btn" onClick ={(event)=>createTask(event)}>
                        Save
                    </button>

                    </div>
                </div>
            </div>
           </div>
        </div>
    )
}

export default New