import React from 'react'
import { useNavigate} from 'react-router-dom'

const Index = () => {
    
        const navigate = useNavigate()

        const newTask = () =>{
            navigate("/task/new")
        }
    
    return (
         
        //title_task

        <div className='container'>
            <div className="tasks_list">
                <div className="titlebar">
                    <div className="titlebar_item">
                        <h2>Tasks</h2>
                    </div>

        {/* click on btn-->new task */}

                    <div className="titlebar_item">
                        <div className="btn" onClick={()=>newTask()}>
                            Add task
                        </div> 
                    </div>  
            </div>

            {/* title list header tab */}
            <div className="table">
                <div className="list_header">
                    <h5>Task</h5>
                    <h5>Description</h5>
                    <h5>Due date</h5>
                    <h5>Status</h5>
                    </div>
                    
                        <div className="list_items">
                            <h5>Title task</h5>
                            <h5>Details</h5>
                            <h5>Deadline</h5>
                            <h5>Report</h5>
                            <div>
                        <button className="btn-icon success">
                            <i className="fas fa-pencil-alt"></i>
                        </button>
                        <button className="btn-icon danger">
                            <i className="fa fa-trash-alt"></i>
                        </button>
                        </div>
                        </div>
                    </div>
                
        </div>
    </div>

    )
}

export default Index