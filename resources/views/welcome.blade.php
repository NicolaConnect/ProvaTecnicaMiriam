<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>To do-app</title>
    @viteReactRefresh
    @vite(['resources/css/app.css', 'resources/js/app.jsx'])
</head>
<body>

    {{-- title --}}
{{-- 
 <div class=" d-flex container justify-content-center mt-3">
    <h1 class="title mx-4">To do app</h1>
    </div> --}}

    {{-- form-to do-list --}}
          {{-- <div class=" mt-3 d-flex container-fluid justify-content-center mt-3">
            <div class="todo-list">
              <div class="list-head">
                <input type= "text" class="entered-list"></input-type>
              <button class="add-list">Add</button>
              </div>

              
              <div class="tasks fs-4 mt-3">
                 <div class="item d-flex d-flex justify-content-between rounded-2 shadow p-3 mb-5">
                  <h4 class="text-light">new item</h4> --}}

                  {{-- buttons + icons --}}
                  {{-- <div class="item-btn">
                    <i class="fa-solid fa-square-pen"></i>
                    <i class="fa-solid fa-square-xmark"></i>
                  </div>
                </div> 
              </div>
              
          </div>
        </div>
      </div> --}}
    
  
      

    <div id="app"></div>
</body>
</html>