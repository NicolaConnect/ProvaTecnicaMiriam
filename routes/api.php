<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\TasksController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/tasks', [ TasksController::class, 'index' ]);
// Route::post('/tasks', [ TasksController::class, 'store' ]);
// Route::get('/tasks/{task}', [ TasksController::class, 'get' ]);
// Route::put('/tasks/{task}', [ TasksController::class, 'update' ]);
// Route::delete('/tasks/{task}', [ TasksController::class, 'destroy' ]);
